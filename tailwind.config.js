module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      colors: {
        neutral: {
          black: "#09101D",
          neutral1: "#2B3A4B",
          neutral2: "#5C5C5C",
          neutral3: "#F3F3F4",
          neutral4: "#E7E7E7"
        },
        primary: {
          blue: "#0055D0",
          yellow: "#FFCC00",
          darkBlue: "#002A76",
          disabled: "#99BBEC"
        }
      },
     
    }
  },
  variants: {},
  plugins: []
};
