import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    redirect: { name: "Login" },
    component: Home
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  // {
  //   path: "/login",
  //   name: "Login",
  //   component: () =>
  //     import(/* webpackChunkName: "login" */ "../views/Login.vue")
  // },
  {
    path: "/login",
    name: "Login",
    redirect:{ name: "LoginStudent" },
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Login.vue"),
    children: [{
      path: "mahasiswa",
      name: "LoginStudent",
      component: () => import(/* webpackChunkName: "cobaMahasiswa" */ "../views/FormStudent.vue")
    },
    {
      path: "korektor",
      name: "LoginCorrector",
      component: () => import(/* webpackChunkName: "cobaKorektor" */ "../views/FormCorrector.vue")
    }]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
